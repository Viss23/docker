import React from 'react';
import { connect } from 'react-redux'
import HeaderChat from './headerChat';
import MessageInput from './addMessage';
import MessageList from './messages/MessageList';
import { getDataDone } from './actions';
import style from '../../mystyle.module.css';
import Loader from '../Loader.js';


class Chat extends React.Component {

  componentDidMount() {
    fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.props.getDataDone(data);
      });
  }

  render() {
    if (this.props.loading) return <Loader />

    return (
      <div className={style.chat}>
        <HeaderChat />
        <MessageList />
        <MessageInput />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.messages.loading
  }
};

const mapDispatchToProps = {
  getDataDone
};


export default connect(mapStateToProps, mapDispatchToProps)(Chat);
