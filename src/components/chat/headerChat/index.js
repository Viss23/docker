import React from 'react';
import style from '../../../mystyle.module.css';
import { connect } from 'react-redux';
import moment from 'moment';

class HeaderChat extends React.Component {


  componentDidMount() {
    this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { lastMessageTime, chatName, participantsAmount, messagesAmount } = this.props;
    let time = moment(lastMessageTime);
    const transformedTime = time.fromNow();

    return (
      <div className={style.headerChat}>
        <div className={style.headerLeft}>
          <span>{chatName}</span>
          <span>{participantsAmount} participants</span>
          <span>{messagesAmount} messages</span>
        </div>
        <div className={style.headerRight}>
          <span>The last message was {transformedTime}</span>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    participantsAmount: state.messages.participantsAmount,
    chatName: state.messages.chatName,
    messagesAmount: state.messages.data.length,
    lastMessageTime: state.messages.data[0].createdAt
  }
};

export default connect(mapStateToProps)(HeaderChat);
