import { SET_CURRENT_USER_ID, DROP_CURRENT_USER_ID, SHOW_PAGE, HIDE_PAGE, UPDATE_TEXT, RESET_TEXT } from "./actionTypes";

const initialState = {
    userId: '',
    isShown: false,
    text: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_USER_ID: {
            const { id } = action.payload;
            return {
                ...state,
                userId: id
            };
        }
        case DROP_CURRENT_USER_ID: {
            return {
                ...state,
                userId: ''
            };
        }

        case SHOW_PAGE: {
            return {
                ...state,
                isShown: true
            };
        }

        case HIDE_PAGE: {
            return {
                ...state,
                isShown: false
            };
        }

        case UPDATE_TEXT: {
            const { text } = action.payload;
            return {
                ...state,
                text
            };
        }

        case RESET_TEXT: {
            return {
                ...state,
                text: ''
            };
        }

        default:
            return state;
    }
}