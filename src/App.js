import React from 'react';
import Chat from './components/chat/Chat';
import Header from './components/common/Header';
import Footer from './components/common/Footer';
import EditModal from './components/chat/editModal'



function App() {
  return (
    <div>
      <Header />
      <Chat />
      <EditModal />
      <Footer />
    </div>
  );
}

export default App;
