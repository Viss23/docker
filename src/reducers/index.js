import { combineReducers } from 'redux';
import messages  from '../components/chat/reducer';
import editModal from '../components/chat/editModal/reducer';
import addMessage from '../components/chat/addMessage/reducer'

const rootReducer = combineReducers({
  messages,
  editModal,
  addMessage
});

export default rootReducer;